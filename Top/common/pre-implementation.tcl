#enable eyescans by default on c2c links
set c2c_prim_cells [get_cells -hierarchical -regexp .*C2C.*_PHY/.*CHANNEL_PRIM_INST]
puts "Found c2c prim cells: $c2c_prim_cells"
foreach cell $c2c_prim_cells {
    puts "Setting ES_EYE_SCAN_EN true on $cell"
    set ret [set_property ES_EYE_SCAN_EN True ${cell}]
}
