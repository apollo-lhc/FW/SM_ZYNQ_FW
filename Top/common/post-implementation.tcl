#enable eyescans by default on c2c links
set c2c_prim_cells [get_cells -hierarchical -regexp .*C2C.*_PHY/.*CHANNEL_PRIM_INST]
puts "Found c2c prim cells: $c2c_prim_cells"
foreach cell $c2c_prim_cells {
    set ret [get_property ES_EYE_SCAN_EN ${cell}]
    puts "ES_EYE_SCAN_EN is $ret on $cell"
}
