<?xml version="1.0" encoding="ISO-8859-1"?>

<node id="TCSDS2_LINK_MASTER_CSR_CONTROL">

  <!-- Full reset of the TCLink. -->
  <node id="RESET_ALL"
        description=""
        address="0x00000000"
        mask="0x00000001"
        permission="rw" />

  <!-- MGT resets. -->
  <node id="MGT_RESET_ALL"
        description="Direct full (i.e., both TX and RX) reset of the MGT. Only enabled when the TCLink channel controller is disabled (i.e., control.tclink_channel_ctrl_enable is low)."
        address="0x00000000"
        mask="0x00000010"
        permission="rw" />
  <node id="MGT_RESET_TX_PLL_AND_DATAPATH"
        description="Direct TX reset of the MGT. Only enabled when the TCLink channel controller is disabled."
        address="0x00000000"
        mask="0x00000020"
        permission="rw" />
  <node id="MGT_RESET_TX_DATAPATH"
        description="Direct TX reset of the MGT. Only enabled when the TCLink channel controller is disabled."
        address="0x00000000"
        mask="0x00000040"
        permission="rw" />
  <node id="MGT_RESET_RX_PLL_AND_DATAPATH"
        description="Direct RX reset of the MGT. Only enabled when the TCLink channel controller is disabled."
        address="0x00000000"
        mask="0x0000080"
        permission="rw" />
  <node id="MGT_RESET_RX_DATAPATH"
        description="Direct RX reset of the MGT. Only enabled when the TCLink channel controller is disabled."
        address="0x00000000"
        mask="0x0000100"
        permission="rw" />

  <!-- Controls for the TCLink channel controller. -->
  <node id="TCLINK_CHANNEL_CTRL_RESET"
        description="Resets the TCLink channel controller."
        address="0x00000000"
        mask="0x00001000"
        permission="rw" />
  <node id="TCLINK_CHANNEL_CTRL_ENABLE"
        description="Enables/disables the TCLink channel controller."
        address="0x00000000"
        mask="0x00002000"
        permission="rw" />
  <node id="TCLINK_CHANNEL_CTRL_GENTLE"
        description="When high: tells the TCLink channel controller to use the 'gentle' instead of the 'full' reset procedure. The 'gentle' procedure does not reset the MGT QUAD PLLs, whereas the 'full' procedure does."
        address="0x00000000"
        mask="0x00004000"
        permission="rw" />

  <!-- Controls for the TCLink core. -->
  <node id="TCLINK_CLOSE_LOOP"
        description="When high: activates the TCLink phase stabilisation."
        address="0x00000000"
        mask="0x00010000"
        permission="rw" />
  <node id="TCLINK_PHASE_OFFSET"
        description="Offset value to be subtracted from TCLink phase error before phase correction. Configure this value from an initial read of status.tclink_phase_error."
        address="0x00000001">
    <node id="LO"
          description="Lower 32 bits of status.tclink_phase_offset."
          address="0x00000000"
          mask="0xffffffff"
          permission="rw" />
    <node id="HI"
          description="Upper 16 bits of status.tclink_phase_offset."
          address="0x00000001"
          mask="0x0000ffff"
          permission="rw" />
  </node>

  <!-- Clock-domain crossing phase control. -->
  <node id="PHASE_CDC40_TX_CALIB"
        description=""
        address="0x00000003"
        mask="0x000003ff"
        permission="rw" />
  <node id="PHASE_CDC40_TX_FORCE"
        description=""
        address="0x00000003"
        mask="0x00000400"
        permission="rw" />
  <node id="PHASE_CDC40_RX_CALIB"
        description=""
        address="0x00000003"
        mask="0x00070000"
        permission="rw" />
  <node id="PHASE_CDC40_RX_FORCE"
        description=""
        address="0x00000003"
        mask="0x00080000"
        permission="rw" />

  <!-- MGT TX Phase-Interpolator control. -->
  <node id="PHASE_PI_TX_CALIB"
        description=""
        address="0x00000003"
        mask="0x7f000000"
        permission="rw" />
  <node id="PHASE_PI_TX_FORCE"
        description=""
        address="0x00000003"
        mask="0x80000000"
        permission="rw" />

  <!-- MGT RX mode (LPM vs. DFE) controls. -->
  <node id="MGT_RX_DFE_VS_LPM"
        description="Choice of MGT mode: 1: LPM, 0: DFE."
        address="0x00000004"
        mask="0x00000001"
        permission="rw" />
  <node id="MGT_RX_DFE_VS_LPM_RESET"
        description="Reset to be strobed after changing MGT RXmode (LPM/DFE)."
        address="0x00000004"
        mask="0x00000002"
        permission="rw" />

  <!-- MGT RX mode parameter overrides. See also:
       https://espace.cern.ch/HighPrecisionTiming/Evaluations/Components%20Evaluations/Xilinx%20FPGAs/report_xilinx_fpga_rx_equalizer.pdf. -->
  <node id="MGT_RXEQ_PARAMS"
        address="0x00000005">
    <node id="LPM"
          address="0x00000000">
      <node id="RXLPMGCOVRDEN"
            address="0x00000000"
            mask="0x00000010"
            permission="rw" />
      <node id="RXLPMHFOVRDEN"
            address="0x00000000"
            mask="0x00000020"
            permission="rw" />
      <node id="RXLPMLFKLOVRDEN"
            address="0x00000000"
            mask="0x00000040"
            permission="rw" />
      <node id="RXLPMOSOVRDEN"
            address="0x00000000"
            mask="0x00000080"
            permission="rw" />
    </node>
    <node id="DFE"
          address="0x00000001">
      <node id="RXOSOVRDEN"
            address="0x00000000"
            mask="0x00000100"
            permission="rw" />
      <node id="RXDFEAGCOVRDEN"
            address="0x00000000"
            mask="0x00000200"
            permission="rw" />
      <node id="RXDFELFOVRDEN"
            address="0x00000000"
            mask="0x00000400"
            permission="rw" />
      <node id="RXDFEUTOVRDEN"
            address="0x00000000"
            mask="0x00000800"
            permission="rw" />
      <node id="RXDFEVPOVRDEN"
            address="0x00000000"
            mask="0x00001000"
            permission="rw" />
    </node>
  </node>

  <!-- Stuff related to link monitoring. -->
  <node id="FEC_MONITOR_RESET"
        description="Reset of the lpGBT FEC monitoring."
        address="0x00000007"
        mask="0x00000001"
        permission="rw" />

  <!-- TCLink parameters. -->
  <node id="TCLINK_PARAM_METASTABILITY_DEGLITCH" address="0x00000008" mask="0x0000ffff" permission="rw" />
  <node id="TCLINK_PARAM_PHASE_DETECTOR_NAVG"    address="0x00000009" mask="0x00000fff" permission="rw" />
  <node id="TCLINK_PARAM_MODULO_CARRIER_PERIOD"  address="0x0000000a">
    <node id="LO" address="0x00000000" mask="0xffffffff" permission="rw" />
    <node id="HI" address="0x00000001" mask="0x0000ffff" permission="rw" />
  </node>
  <node id="TCLINK_PARAM_MASTER_RX_UI_PERIOD" address="0x0000000c">
    <node id="LO" address="0x00000000" mask="0xffffffff" permission="rw" />
    <node id="HI" address="0x00000001" mask="0x0000ffff" permission="rw" />
  </node>
  <node id="TCLINK_PARAM_AIE"                 address="0x0000000e" mask="0x0000000f" permission="rw" />
  <node id="TCLINK_PARAM_AIE_ENABLE"          address="0x0000000e" mask="0x00000010" permission="rw" />
  <node id="TCLINK_PARAM_APE"                 address="0x0000000e" mask="0x00000f00" permission="rw" />
  <node id="TCLINK_PARAM_SIGMA_DELTA_CLK_DIV" address="0x0000000f" mask="0xffff0000" permission="rw" />
  <node id="TCLINK_PARAM_ENABLE_MIRROR"       address="0x00000010" mask="0x00000001" permission="rw" />
  <node id="TCLINK_PARAM_ADCO" address="0x00000011">
    <node id="LO" address="0x00000000" mask="0xffffffff" permission="rw" />
    <node id="HI" address="0x00000001" mask="0x0000ffff" permission="rw" />
  </node>

</node>
